// Code generated by protoc-gen-micro. DO NOT EDIT.
// source: user/user_micro.proto

package user_micro

import (
	fmt "fmt"
	proto "google.golang.org/protobuf/proto"
	math "math"
)

import (
	context "context"
	api "go-micro.dev/v4/api"
	client "go-micro.dev/v4/client"
	server "go-micro.dev/v4/server"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// Reference imports to suppress errors if they are not otherwise used.
var _ api.Endpoint
var _ context.Context
var _ client.Option
var _ server.Option

// Api Endpoints for UserService service

func NewUserServiceEndpoints() []*api.Endpoint {
	return []*api.Endpoint{}
}

// Client API for UserService service

type UserService interface {
	LoginServer(ctx context.Context, in *LoginInput, opts ...client.CallOption) (*LoginOutPut, error)
}

type userService struct {
	c    client.Client
	name string
}

func NewUserService(name string, c client.Client) UserService {
	return &userService{
		c:    c,
		name: name,
	}
}

func (c *userService) LoginServer(ctx context.Context, in *LoginInput, opts ...client.CallOption) (*LoginOutPut, error) {
	req := c.c.NewRequest(c.name, "UserService.LoginServer", in)
	out := new(LoginOutPut)
	err := c.c.Call(ctx, req, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for UserService service

type UserServiceHandler interface {
	LoginServer(context.Context, *LoginInput, *LoginOutPut) error
}

func RegisterUserServiceHandler(s server.Server, hdlr UserServiceHandler, opts ...server.HandlerOption) error {
	type userService interface {
		LoginServer(ctx context.Context, in *LoginInput, out *LoginOutPut) error
	}
	type UserService struct {
		userService
	}
	h := &userServiceHandler{hdlr}
	return s.Handle(s.NewHandler(&UserService{h}, opts...))
}

type userServiceHandler struct {
	UserServiceHandler
}

func (h *userServiceHandler) LoginServer(ctx context.Context, in *LoginInput, out *LoginOutPut) error {
	return h.UserServiceHandler.LoginServer(ctx, in, out)
}
